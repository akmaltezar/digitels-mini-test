export const Main_Color = '#0b2545';
export const Disable_Color = '#CDD2DA';
export const Secondary_Color = '#8da9c4';
export const Error_Color = '#c44536';
export const Success_Color = '#4c956c';
