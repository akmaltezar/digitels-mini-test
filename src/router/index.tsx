import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

import {
  Menu,
  Products,
  InputValidation,
  ImagePicker,
  ProductDetail,
} from '../screens';

const Stack = createNativeStackNavigator();

const Router = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="menu"
          component={Menu}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="products"
          component={Products}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="inputvalidation"
          component={InputValidation}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="imagepicker"
          component={ImagePicker}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="productdetail"
          component={ProductDetail}
          options={{headerShown: false}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Router;
