import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import React from 'react';
import type {PropsWithChildren} from 'react';

type OptionProps = PropsWithChildren<{
  title: string;
  onPress: any;
}>;

function Option({title, onPress}: OptionProps): JSX.Element {
  return (
    <TouchableOpacity style={styles.bottom_Menu} onPress={onPress}>
      <Text style={styles.text}>{title}</Text>
    </TouchableOpacity>
  );
}

export default Option;

const styles = StyleSheet.create({
  bottom_Menu: {
    borderWidth: 2,
    // borderColor: ,
    paddingHorizontal: '20%',
    paddingVertical: 5,
    borderRadius: 7,
    margin: 10,
  },
  text: {
    fontSize: 22,
    fontWeight: '700',
    textAlign: 'center',
    // color: ,
  },
});
