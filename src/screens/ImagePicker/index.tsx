import {
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  PermissionsAndroid,
  ToastAndroid,
  Image,
} from 'react-native';
import React, {useState, useEffect} from 'react';
import {launchCamera} from 'react-native-image-picker';

const ImagePicker = () => {
  const [data, setData] = React.useState(null);

  const [save, setSave] = useState(false);

  const saveImage = () => {
    setSave(true);

    ToastAndroid.show('Image Successfully Saved', ToastAndroid.SHORT);
  };
  const requestCameraPermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CAMERA,
        {
          title: 'Technical App Camera Permission',
          message: 'This app needs access to your camera',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        openCamera();
        console.log('Camera permission given');
      } else {
        ToastAndroid.show(
          'Go to App Info to allow the permission',
          ToastAndroid.SHORT,
        );
        console.log('Camera permission denied');
      }
    } catch (err) {
      console.warn(err);
    }
  };

  const openCamera = () => {
    const options: any = {
      saveToPhotos: save,
      mediaType: 'photo',
      includeBase64: false,
    };
    launchCamera(options, res => {
      if (res.didCancel) {
        console.log('User canceled camera');
      } else if (res.errorCode) {
        console.log(res.errorMessage);
      } else {
        setData(res.assets[0].uri);
      }
    });
  };

  useEffect(() => {
    requestCameraPermission();
  }, []);
  return (
    <View style={styles.container}>
      <Image
        source={{uri: data}}
        style={{
          height: 200,
          width: 200,
          marginVertical: 15,
        }}
      />

      <TouchableOpacity onPress={() => openCamera()}>
        <Text>Open Camera</Text>
      </TouchableOpacity>
      {data !== null ? (
        <TouchableOpacity onPress={() => saveImage()}>
          <Text>Save Image</Text>
        </TouchableOpacity>
      ) : (
        ''
      )}
    </View>
  );
};

export default ImagePicker;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
