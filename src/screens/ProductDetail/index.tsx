import {StyleSheet, Text, View, ScrollView, Image} from 'react-native';
import React, {useState, useEffect} from 'react';

const ProductDetail = ({route}: any) => {
  const {productId} = route.params;

  const [data, setData] = useState({});
  const [loading, setLoading] = useState(false);

  const fetchProducts = () => {
    setLoading(true);
    var requestOptions = {
      method: 'GET',
      redirect: 'follow',
    };

    fetch(`https://dummyjson.com/products/${productId}`, requestOptions)
      .then(response => response.json())
      .then(result => {
        setData(result);
        setLoading(false);
      })
      .catch(error => console.log('error', error));
    setLoading(false);
  };
  useEffect(() => {
    fetchProducts();
  }, []);
  return (
    <ScrollView>
      <View style={styles.product}>
        <Image
          source={{
            uri: data.thumbnail,
          }}
          style={styles.productImg}
        />
        <Text style={styles.title}>{data.title}</Text>
        <View style={styles.productData}>
          <Text style={styles.price}> ${data.price} </Text>
          <Text style={styles.rating}> ★{data.rating} </Text>
        </View>
        <Text style={styles.description}>{data.description}</Text>
        <View style={styles.category}>
          <Text style={styles.category}>{data.brand}</Text>
          <Text style={styles.category}>{data.category}</Text>
        </View>
      </View>
    </ScrollView>
  );
};

export default ProductDetail;

const styles = StyleSheet.create({
  product: {
    backgroundColor: 'white',
    borderRadius: 5,
    elevation: 3,
    margin: '2%',
    padding: 20,
  },
  productImg: {
    width: 200,
    height: 200,
  },
  productData: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
  },
  title: {
    fontWeight: '700',
    fontSize: 18,
    color: '#000',
    marginBottom: 15,
  },
  price: {
    fontWeight: '700',
    fontSize: 18,
    color: '#38b000',
    marginBottom: 15,
  },
  rating: {
    fontWeight: '700',
    fontSize: 18,
    color: '#fec601',
    marginBottom: 15,
  },
  description: {},
  category: {},
});
