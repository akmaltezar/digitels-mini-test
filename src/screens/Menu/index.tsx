import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import Option from '../../components/OptionButton';
import {useNavigation} from '@react-navigation/native';

const Menu = () => {
  const {navigate} = useNavigation();
  return (
    <View style={styles.container}>
      <Option title="Products" onPress={() => navigate('products')} />

      <Option
        title="Input Validation"
        onPress={() => navigate('inputvalidation')}
      />
      <Option title="Image Picker" onPress={() => navigate('imagepicker')} />
    </View>
  );
};

export default Menu;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#FFFFFF',
  },
});
