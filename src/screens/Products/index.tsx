import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import {useState, useEffect} from 'react';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Loading from '../../components/Loading';
import {ScrollView} from 'react-native-gesture-handler';
import {useNavigation} from '@react-navigation/native';

const Products = ({navigation}: any) => {
  const [loading, setLoading] = useState(false);
  const [products, setProducts] = useState([]);
  const [query, setQuery] = useState('');

  const searchProduct = () => {
    var requestOptions = {
      method: 'GET',
      redirect: 'follow',
    };

    fetch(`https://dummyjson.com/products/search?q=${query}`, requestOptions)
      .then(response => response.json())
      .then(result => {
        // console.log(result);
        setProducts(result.products);
      })
      .catch(error => console.log('error', error));
  };

  const fetchProducts = () => {
    setLoading(true);
    var requestOptions = {
      method: 'GET',
      redirect: 'follow',
    };

    fetch('https://dummyjson.com/products', requestOptions)
      .then(response => response.json())
      .then(result => {
        // console.log(result)
        setProducts(result.products);
        setLoading(false);
      })
      .catch(error => console.log('error', error));
    setLoading(false);
  };

  useEffect(() => {
    fetchProducts();
  }, []);
  useEffect(() => {
    searchProduct();
  }, [query]);
  return (
    <ScrollView style={styles.container}>
      <View style={styles.searchBox}>
        <TextInput
          style={styles.searchInput}
          onChangeText={text => setQuery(text)}
        />
        <TouchableOpacity style={styles.searchBtn}>
          <Text style={styles.magnify}>🔍</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.productContainer}>
        {products.map((item: any) => {
          return (
            <TouchableOpacity
              key={item.id}
              style={styles.product}
              onPress={() =>
                navigation.navigate('productdetail', {
                  productId: item.id,
                })
              }>
              <Image
                source={{
                  uri: item.thumbnail,
                }}
                style={styles.productImg}
              />

              <Text style={styles.title}> {item.title} </Text>
              <View style={styles.productData}>
                <Text style={styles.price}> ${item.price} </Text>
                <Text style={styles.rating}> ★{item.rating} </Text>
              </View>
            </TouchableOpacity>
          );
        })}
      </View>
    </ScrollView>
  );
};

export default Products;

const styles = StyleSheet.create({
  container: {
    padding: 10,
  },
  searchBox: {
    flexDirection: 'row',
    width: '90%',
    marginLeft: 10,
    backgroundColor: 'white',
    alignItems: 'center',
    borderRadius: 5,
    elevation: 3,
  },
  searchInput: {
    width: '85%',
    padding: 10,
    fontSize: 20,
  },
  searchBtn: {
    width: '10%',
  },
  magnify: {
    fontSize: 25,
  },
  productContainer: {
    padding: 7,
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  product: {
    width: '44%',
    backgroundColor: 'white',
    alignItems: 'center',
    borderRadius: 5,
    elevation: 3,
    margin: '2%',
    padding: 10,
  },
  productImg: {
    width: 100,
    height: 100,
  },
  productData: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
  },
  title: {
    fontWeight: '700',
    fontSize: 18,
    color: '#000',
    textAlign: 'center',
    marginBottom: 15,
  },
  price: {
    fontWeight: '700',
    fontSize: 18,
    color: '#38b000',
    textAlign: 'center',
    marginBottom: 15,
  },
  rating: {
    fontWeight: '700',
    fontSize: 18,
    color: '#fec601',
    textAlign: 'center',
    marginBottom: 15,
  },
});
